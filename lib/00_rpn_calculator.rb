class RPNCalculator
  def initialize
    @stack = []
  end

  def push(num)
    @stack << num
  end

  def value
    @stack.last
  end

  def plus
    perform_operation(:+)
  end

  def minus
    perform_operation(:-)
  end

  def divide
    perform_operation(:/)
  end

  def times
    perform_operation(:*)
  end

  def tokens(string)
    tokens = string.split
    tokens.map do |char|
      if operation?(char)
        char.to_sym
      else
        char.to_i
      end
    end
  end

  def evaluate(string)
    tokens = tokens(string)
    tokens.each do |token|
      if token.is_a? Numeric
        push(token)
      else
        perform_operation(token)
      end
    end
    value
  end

  private

  def operation?(char)
    [:+, :-, :*, :/].include?(char.to_sym)
  end


  def perform_operation(op_symbol)

    raise "calculator is empty" if @stack.size < 2

    second = @stack.pop
    first = @stack.pop

    case op_symbol
    when :+
      @stack << first + second
    when :-
      @stack << first - second
    when :*
      @stack << first * second
    when :/
      @stack << first/second.to_f
    else
      @stack << first
      @stack << second
      put "No such operation: #{op_symbol}"
    end
  end
end
